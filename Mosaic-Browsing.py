import sys #importing sys module

def no_of_occurrences(row1, col1, motif, row2, col2, mosaic):
    occurrences = []
    count = 0 #count initialized to zero
    for i in range(row2 - row1 + 1): # loop which traverse through mosaic
        for j in range(col2 - col1 + 1):
            match = True
            for x in range(row1): #loop which traverse through motif
                for y in range(col1):
                    if motif[x][y] != 0 and motif[x][y] != mosaic[i + x][j + y]:
                        match = False
                        break #used to break the loop
                if not match:
                    break
            if match:
                 count +=1
                 occurences.append((i + 1,j + 1)) #appends the positions of top-left side of mosaic by using append() method
    return  [count] + occurrences #returned list which contains count and no_of_occurnces

'''row1 = 2
col1 = 2
row2 = 3
col2 = 4'''
row1, col1, row2, col2 = int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]) #reads the input values
motif = ([1, 0], [0, 1])
mosaic = ([1, 2, 1, 2], [2, 1, 1, 1], [2, 2, 1, 3])

print(no_of_occurrences(row1, col1, motif, row2, col2, mosaic))



